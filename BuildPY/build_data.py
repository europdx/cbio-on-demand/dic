#!/usr/bin/env python2.7
import os

# bere ID jako argument, predava dal
import sys


prefix = "/home/pdxuser/" + sys.argv[1] + "/"
prefix2 = "/home/pdxuser/" + sys.argv[1] + "/"
prefix3 = prefix + "dic/BuildPY/"
study = sys.argv[2] + "/"
outputpath = "../../inject_data/" + study

os.system(prefix3 + "build.py " + prefix + "data/" + study + "data_clinical_patients.json " +
          outputpath + "data_clinical_patients.txt " + ' '.join(sys.argv[3:]))
os.system(prefix3 + "build.py " + prefix + "data/" + study + "data_clinical_samples.json " +
          outputpath + "data_clinical_samples.txt " + ' '.join(sys.argv[3:]))
os.system(prefix3 + "id_samples_helper.py " + prefix + "data/" + study + "data_clinical_samples.json " + prefix2 +
          "data/" + study + "samples_id.txt " + ' '.join(sys.argv[3:]))


with open("/home/pdxuser/" + sys.argv[1] + "/data/" + study + "samples_id.txt", "r") as samples_id:
    samples = samples_id.readline()
    samples = " ".join(list(set(samples.split(" "))))
    samples_id.close()
    os.system(prefix3 + "build_expression.py " + prefix + "data/" + study + "data_expression_file_Linear.json " +
              outputpath + "data_expression_file_Linear.txt " + samples)
    os.system(prefix3 + "build_expression.py " + prefix + "data/" + study + "data_expression_file_Log2.json " +
              outputpath + "data_expression_file_Log2.txt " + samples)
    os.system(prefix3 + "build_expression.py " + prefix + "data/" + study + "data_expression_file_Zscore.json " +
              outputpath + "data_expression_file_Zscore.txt " + samples)
    os.system(prefix3 + "build_expression.py " + prefix + "data/" + study + "data_discreteCNA_rtpcr.json " +
              outputpath + "data_discreteCNA_rtpcr.txt " + samples)
    os.system(prefix3 + "build_expression.py " + prefix + "data/" + study + "data_discreteCNA_wes.json " +
              outputpath + "data_discreteCNA_wes.txt " + samples)
    os.system(prefix3 + "crc_build.py " + study + " " + samples)
