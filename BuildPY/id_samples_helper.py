#!/usr/bin/env python2.7

import json
import sys
"""
first argument - input path
second argument - output path 
third + argument - ids
"""
with open(sys.argv[1]) as file:
    dictionary = json.load(file)
    print(sys.argv[2])
    resultFile = open(sys.argv[2], 'w')
    not_found_file = open("notfound.txt", 'w')
    # resultFile.write('\n')
    for i in range(3, len(sys.argv)):
        if dictionary.get(sys.argv[i]) is not None:
            resultFile.write(dictionary[sys.argv[i]].split('\t')[1] + " ")
        else:
            not_found_file.write(sys.argv[i] + "\n")

    not_found_file.close()
    resultFile.close()
