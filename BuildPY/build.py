#!/usr/bin/env python2.7
import json
import sys

"""
first argument - input path
second argument - output path 
third + argument - ids
Script takes name of dictionary in json format as first argument. (e.g data_clinical_patients.json)
and makes a text file (e.g data_clinical_patient.txt) with id provided as another arguments
"""

id_not_found_file_name = sys.argv[1].split(".")[0] + "_id_not_found.txt"

with open(sys.argv[1]) as file, open(id_not_found_file_name, "w") as id_not_found_file:
    dictionary = json.load(file)

    resultFile = open(sys.argv[2], 'w')
    resultFile.write(dictionary['header'])

    for i in range(3, len(sys.argv)):
        if dictionary.get(sys.argv[i]) is not None:
            resultFile.write(dictionary[sys.argv[i]])
        else:
            id_not_found_file.write(sys.argv[i] + "\n")

    resultFile.close()
