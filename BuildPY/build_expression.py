#!/usr/bin/env python2.7
import json
import os
import sys

"""
first argument - input path
second argument - output path 
Build data_expression_file - s(3) 
First argv is name of file , then ids follow
"""


with open(sys.argv[1]) as file:
    resultFile = open(sys.argv[2], 'w')

    dic = json.load(file)
    canWrite = False
    
    for i in range(1,len(dic)):
        resultFile.write(dic[str(i)]['header'])
        resultFile.write('\t')

        for j in range(2,len(sys.argv) - 1):
            if dic[str(i)].get(sys.argv[j]) is not None:
                canWrite = True
                resultFile.write(dic[str(i)][sys.argv[j]] + '\t')

        if dic[str(i)].get(len(sys.argv) - 1) is not None:
            canWrite = True
            resultFile.write(dic[str(i)][sys.argv[j+1]])
            resultFile.write('\n')
        else:
            resultFile.seek(-1, 1)
            resultFile.write('\n')

    resultFile.close()
    if canWrite is False:
        os.remove(sys.argv[2])
