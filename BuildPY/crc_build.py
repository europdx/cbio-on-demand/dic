#!/usr/bin/env python2.7
import csv
import sys

"""
first argument - study name
second + argument - ids 
Builds crc_wes+sanger3.csv file
"""

with open('../../data/' + sys.argv[1] + 'crc_wes+sanger3.csv') as file, open('../../inject_data/' +
        sys.argv[1] + 'crc_wes+sanger3.csv', 'w') as resultFile:
    reader = csv.reader(file)
    writer = csv.writer(resultFile)

    for line in reader:
        lineS = line[0].split('\t')
        if lineS[7] in sys.argv or lineS[7] == 'Tumor_Sample_Barcode':
            writer.writerow(line)


